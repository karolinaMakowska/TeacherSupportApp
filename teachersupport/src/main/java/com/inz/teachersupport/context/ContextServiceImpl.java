package com.inz.teachersupport.context;
import com.inz.teachersupport.person.Person;
import java.io.IOException;
import java.nio.file.Files;
import com.inz.teachersupport.personSecurity.UserSecurityData;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import java.io.File;
import java.util.Base64;

@Service
public class ContextServiceImpl implements IContextService {

    @Override
    public void homeContext(Context context, IServiceProvider serviceProvider) {
        context.clearVariables();
        Person person = serviceProvider.getIPersonService().getCurrentPerson(serviceProvider);
        context.setVariable("news", person.getPersonNewsList());
        context.setVariable("who", person);
        String pictureCode = getDefaultLogo();
        if (person.getFacultyField() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFacultyField().getFile().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("facultyPhoto", pictureCode);
    }

    @Override
    public void aboutMeContext(Context context,IServiceProvider serviceProvider) {
        context.clearVariables();
        Person person = serviceProvider.getIPersonService().getCurrentPerson(serviceProvider);
        context.setVariable("who", person);
        String pictureCode = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABkAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCOiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//Z";
        if (person.getFoto() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFoto().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("photo", pictureCode);

        pictureCode = getDefaultLogo();
        if (person.getFacultyField() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFacultyField().getFile().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("facultyPhoto", pictureCode);
        String cVCode = "";
        if (person.getCV() != null) {
            cVCode = Base64.getEncoder().encodeToString(person.getCV().getPic());
            cVCode = "data:application/pdf;base64," + cVCode;
        }
        context.setVariable("cv", cVCode);
        context.setVariable("name", person.getNameField());
        context.setVariable("surname", person.getSurnameField());
        context.setVariable("currentUserPerson", person);
        context.setVariable("currentUserName", person.getUserSecurityDataField().getEmail());
    }

    @Override
    public void publicationContext(Context context,IServiceProvider serviceProvider) {
        context.clearVariables();
        Person person = serviceProvider.getIPersonService().getCurrentPerson(serviceProvider);
        context.setVariable("who", person);
        context.setVariable("publication", person.getPersonPublicationList());
        String pictureCode = getDefaultLogo();
        if (person.getFacultyField() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFacultyField().getFile().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("facultyPhoto", pictureCode);
    }

    @Override
    public void studentContext(Context context,IServiceProvider serviceProvider) {
        context.clearVariables();
        Person person = serviceProvider.getIPersonService().getCurrentPerson(serviceProvider);
        context.setVariable("generatorPersonAllGroups", person.getPersonStudGroupList());
        context.setVariable("who", person);
        String pictureCode = getDefaultLogo();
        if (person.getFacultyField() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFacultyField().getFile().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("facultyPhoto", pictureCode);
    }

    @Override
    public void contactContext(Context context,IServiceProvider serviceProvider) {
        context.clearVariables();
        Person person = serviceProvider.getIPersonService().getCurrentPerson(serviceProvider);
        context.setVariable("meetMeDataList", person.getPersonMeetMeDataList());
        context.setVariable("who", person);
        String pictureCode = getDefaultLogo();
        if (person.getFacultyField() != null) {
            pictureCode = Base64.getEncoder().encodeToString(person.getFacultyField().getFile().getPic());
            pictureCode = "data:image/jpeg;base64," + pictureCode;
        }
        context.setVariable("facultyPhoto", pictureCode);

    }

    @Override
    public void nullContext(Context context) {
        context.clearVariables();
        Person person = new Person();
        UserSecurityData userSecurityData = new UserSecurityData();
        person.setUserSecurityDataField(userSecurityData);
        String pictureCode = getDefaultLogo();
        context.setVariable("who", person);
        context.setVariable("news", person.getPersonNewsList());
        context.setVariable("facultyPhoto", pictureCode);
        context.setVariable("photo", null);
        context.setVariable("name", null);
        context.setVariable("surname", null);
        context.setVariable("currentUserPerson", null);
        context.setVariable("currentUserName", null);
        context.setVariable("publication", person.getPersonPublicationList());
        context.setVariable("generatorPersonAllGroups", person.getPersonStudGroupList());
        context.setVariable("meetMeDataList", person.getPersonMeetMeDataList());
    }
    public String getDefaultLogo()
    {
        File file=new File("src\\main\\resources\\static\\images\\logo.jpg");
        String pictureCode = "data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAIBAQIBAQICAgICAgICAwUDAwMDAwYEBAMFBwYHBwcGBwcICQsJCAgKCAcHCg0KCgsMDAwMBwkODw0MDgsMDAz/2wBDAQICAgMDAwYDAwYMCAcIDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAz/wAARCABkAGQDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwCOiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigD//Z";
        try {
            byte[] fileContent = Files.readAllBytes(file.toPath());
            pictureCode="data:image/jpeg;base64,"+Base64.getEncoder().encodeToString(fileContent);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return pictureCode;
    }
}
