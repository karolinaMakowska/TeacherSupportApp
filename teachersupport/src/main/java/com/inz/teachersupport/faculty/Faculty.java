package com.inz.teachersupport.faculty;

import com.inz.teachersupport.file.File;
import com.inz.teachersupport.person.Person;
import org.apache.logging.log4j.util.Strings;
import javax.persistence.*;
import java.util.List;

@Entity
public class Faculty {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Version
    private Integer version;

    private String facultyNameField;

    @OneToMany(mappedBy = "facultyField")
    private List<Person> facultyAndPersonList;

    @OneToOne(fetch = FetchType.LAZY, cascade = {CascadeType.ALL})
    private File file;

    public Faculty() {
        this.facultyNameField = Strings.EMPTY;

    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getFacultyNameField() {
        return facultyNameField;
    }

    public void setFacultyNameField(String facultyNameField) {
        this.facultyNameField = facultyNameField;
    }

    public List<Person> getFacultyAndPersonList() {
        return facultyAndPersonList;
    }

    public void setFacultyAndPersonList(List<Person> facultyAndPersonList) {
        this.facultyAndPersonList = facultyAndPersonList;
    }

    public void addPersonToFaculty(Person person) {
        this.facultyAndPersonList.add(person);
        if (person.getFacultyField() != this) {
            person.setFacultyField(this);
        }
    }


}