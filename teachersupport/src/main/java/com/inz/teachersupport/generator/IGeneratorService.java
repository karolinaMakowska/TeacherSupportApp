package com.inz.teachersupport.generator;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;
import java.util.zip.ZipOutputStream;

public interface IGeneratorService {
    MultipartFile generate(String listOfPages, IServiceProvider serviceProvider) throws Exception;
    MultipartFile generateTemplates(IServiceProvider serviceProvider) throws Exception;
    void addPageToOutput(ZipOutputStream out, String pageName, Context context);
}
