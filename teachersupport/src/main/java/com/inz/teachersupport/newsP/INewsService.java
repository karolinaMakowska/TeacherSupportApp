package com.inz.teachersupport.newsP;

import com.inz.teachersupport.person.Person;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import java.util.List;

public interface INewsService
{
    void addNews(News news, IServiceProvider serviceProvider);
    News goEditNews(EditNewsDTO editNewsDTO,IServiceProvider serviceProvider);
    void deleteNewsByContent(String newsContent,IServiceProvider serviceProvider);
    List<News> cleanMyNews(Person person,IServiceProvider serviceProvider);
}
