package com.inz.teachersupport.publication;

import com.inz.teachersupport.person.Person;
import com.inz.teachersupport.serviceProvider.IServiceProvider;
import java.util.List;

public interface IPublicationService {
    Publication goEditPublication(EditPublicationDTO editPublicationDTO, IServiceProvider serviceProvider);

    void deletePublicationByContent(String publiContent,IServiceProvider serviceProvider);

    List<Publication> cleanMyPublications(Person person, IServiceProvider serviceProvider);

    void addNewPublication(Publication publication,IServiceProvider serviceProvider);
}
