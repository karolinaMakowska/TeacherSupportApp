package com.inz.teachersupport.serviceProvider;

import com.inz.teachersupport.context.IContextService;
import com.inz.teachersupport.faculty.IFacultyService;
import com.inz.teachersupport.file.IFileService;
import com.inz.teachersupport.fileStorage.IFileStorageService;
import com.inz.teachersupport.generator.IGeneratorService;
import com.inz.teachersupport.modelAttributeProvider.IModelAttributeProviderService;
import com.inz.teachersupport.newsP.INewsService;
import com.inz.teachersupport.person.IMeetMeService;
import com.inz.teachersupport.person.IPersonService;
import com.inz.teachersupport.personSecurity.IUserSecurityDataService;
import com.inz.teachersupport.personSecurity.personEditProfile.IEditProfileService;
import com.inz.teachersupport.personSecurity.personRegister.verificationToken.IVerificationTokenService;
import com.inz.teachersupport.publication.IPublicationService;
import com.inz.teachersupport.roles.IRoleService;
import com.inz.teachersupport.studGroup.IGroupRemoteResourceService;
import com.inz.teachersupport.studGroup.IStudGroupService;

public interface IServiceProvider {
    IPersonService getIPersonService();
    IUserSecurityDataService getIUserSecurityDataService();
    IFacultyService getIFacultyService();
    IMeetMeService getIMeetMeService();
    IRoleService getIRoleService();
    IStudGroupService getIStudGroupService();
    IPublicationService getIPublicationService();
    INewsService getINewsService();
    IGroupRemoteResourceService getIGroupRemoteResourceService();
    IVerificationTokenService getITokenService();
    IEditProfileService getIEditProfileService();
    IModelAttributeProviderService getIModelService();
    IFileService getIFileService();
    IFileStorageService getIFileStorage();
    IContextService getIContextService();
    IGeneratorService getIGeneratorService();
}
